﻿using MagicVilla.DAL.Core;
using MagicVilla.DAL.Entities;

namespace MagicVilla.DAL.Interfaces
{
    public interface IVillaNumberRepository : IRepository<VillaNumber> {}
}
