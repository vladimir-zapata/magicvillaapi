﻿using MagicVilla.DAL.Entities;
using System.Linq.Expressions;

namespace MagicVilla.DAL.Core
{
    public interface IRepository<TEntity> where TEntity : class
    {
        Task<TEntity?> GetEntityAsync(Expression<Func<TEntity, bool>>? filter = null, bool tracked = true, string? includeProperties = null);
        Task<List<TEntity>> GetEntitiesAsync(Expression<Func<TEntity, bool>>? filter = null, string? includeProperties = null, int pageSize = 0, int pageNumber = 1);
        Task<bool> ExistsAsync(Expression<Func<TEntity, bool>> filter);
        Task SaveAsync(TEntity entity);
        Task SaveAsync(TEntity[] entities);
        Task UpdateAsync(TEntity entity);
        Task UpdateAsync(TEntity[] entities);
        Task RemoveAsync(TEntity entity);
        Task RemoveAsync(TEntity[] entities);
        Task SaveChangesAsync();
    }
}
