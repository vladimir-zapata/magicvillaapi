﻿using MagicVilla.DAL.Context;

namespace MagicVilla.DAL.Core
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly MagicVillaContext _context;
        private Dictionary<Type, object> _repositories;

        public UnitOfWork(MagicVillaContext context)
        {
            _context = context;
            _repositories = new Dictionary<Type, object>();
        }

        public IRepository<T> GetRepository<T>() where T : class
        {
            if (_repositories.ContainsKey(typeof(T)))
            {
                return (IRepository<T>)_repositories[typeof(T)];
            }

            IRepository<T> repository = new Repository<T>(_context);
            _repositories.Add(typeof(T), repository);

            return repository;
        }

        public virtual async ValueTask DisposeAsync()
        {
            await _context.DisposeAsync();
        }
    }
}
