﻿using MagicVilla.DAL.Context;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace MagicVilla.DAL.Core
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private readonly MagicVillaContext _context;
        private readonly DbSet<TEntity> _dbSet;

        public Repository(MagicVillaContext context)
        {
            _context = context;
            _dbSet = _context.Set<TEntity>();
        }

        public virtual async Task<List<TEntity>> GetEntitiesAsync(Expression<Func<TEntity, bool>>? filter = null, string? includeProperties = null, int pageSize = 3, int pageNumber = 1)
        {
            IQueryable<TEntity> query = _dbSet;

            if (filter != null) query = query.Where(filter);

            if (pageSize > 0)
            {
                if (pageSize > 100) pageSize = 100;

                query = query.Skip(pageSize * (pageNumber - 1)).Take(pageSize);
            }

            if (includeProperties != null)
            {
                foreach (var includeProp in includeProperties.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    query = query.Include(includeProp);
                }
            }

            return await query.ToListAsync();
        }
        public virtual async Task<TEntity?> GetEntityAsync(Expression<Func<TEntity, bool>>? filter = null, bool tracked = true, string? includeProperties = null)
        {
            IQueryable<TEntity> query = _dbSet;

            if (!tracked) query = query.AsNoTracking();

            if (filter != null) query = query.Where(filter);

            if (includeProperties != null)
            {
                foreach (var includeProp in includeProperties.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    query = query.Include(includeProp);
                }
            }

            return await query.FirstOrDefaultAsync();
        }
        public virtual async Task<bool> ExistsAsync(Expression<Func<TEntity, bool>> filter)
        {
            return await _dbSet.AnyAsync(filter);
        }
        public virtual async Task SaveAsync(TEntity entity)
        {
            await _dbSet.AddAsync(entity);
            await SaveChangesAsync();
            
        }
        public virtual async Task SaveAsync(TEntity[] entities)
        {
            await _dbSet.AddRangeAsync(entities);
            await SaveChangesAsync();
        }
        public virtual async Task UpdateAsync(TEntity entity)
        {
            _dbSet.Update(entity);
            await SaveChangesAsync();
        }
        public virtual async Task UpdateAsync(TEntity[] entities)
        {
            _dbSet.UpdateRange(entities);
            await SaveChangesAsync();
        }
        public virtual async Task RemoveAsync(TEntity entity)
        {
            _dbSet.Remove(entity);
            await SaveChangesAsync();
        }
        public virtual async Task RemoveAsync(TEntity[] entities)
        {
            _dbSet.RemoveRange(entities);
            await SaveChangesAsync();
        }
        public virtual async Task SaveChangesAsync()
        {
            await _context.SaveChangesAsync();
        }
    }
}
