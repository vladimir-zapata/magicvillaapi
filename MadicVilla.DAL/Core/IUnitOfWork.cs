﻿namespace MagicVilla.DAL.Core
{
    public interface IUnitOfWork : IAsyncDisposable
    {
        IRepository<T> GetRepository<T>() where T : class;
    }

}
