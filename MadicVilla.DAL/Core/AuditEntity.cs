﻿namespace MagicVilla.DAL.Core
{
    public abstract class AuditEntity
    {
        public DateTime CreationDate { get; set; }
        public string CreationUser { get; set; } = string.Empty;
        public string? ModifyUser { get; set; }
        public DateTime? ModifyDate { get; set; }
        public string? DeleteUser { get; set; }
        public DateTime? DeleteDate { get; set; }
        public bool Deleted { get; set; }

        public AuditEntity()
        {
            CreationDate = DateTime.Now;
            Deleted = false;
        }
    }
}
