﻿using MagicVilla.DAL.Core;

namespace MagicVilla.DAL.Entities
{
    public class User : AuditEntity
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
    }
}
