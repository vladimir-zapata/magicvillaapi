﻿using Microsoft.AspNetCore.Identity;

namespace MagicVilla.DAL.Entities
{
    public class ApplicationUser : IdentityUser
    {
        public string? Name { get; set; }
    }
}
