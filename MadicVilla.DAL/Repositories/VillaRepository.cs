﻿using MagicVilla.DAL.Context;
using MagicVilla.DAL.Core;
using MagicVilla.DAL.Entities;
using MagicVilla.DAL.Interfaces;

namespace MagicVilla.DAL.Repositories
{
    public class VillaRepository : Repository<Villa>, IVillaRepository
    {
        private readonly MagicVillaContext _context;
        public VillaRepository(MagicVillaContext context) : base(context)
        {
            _context = context;
        }
    }
}
