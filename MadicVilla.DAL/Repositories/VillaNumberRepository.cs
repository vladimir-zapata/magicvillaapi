﻿using MagicVilla.DAL.Context;
using MagicVilla.DAL.Core;
using MagicVilla.DAL.Entities;
using MagicVilla.DAL.Interfaces;

namespace MagicVilla.DAL.Repositories
{
    public class VillaNumberRepository : Repository<VillaNumber>, IVillaNumberRepository
    {
        private readonly MagicVillaContext _context;

        public VillaNumberRepository(MagicVillaContext context) : base(context)
        {
            _context = context;
        }
    }
}
