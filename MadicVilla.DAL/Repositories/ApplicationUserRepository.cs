﻿using MagicVilla.DAL.Context;
using MagicVilla.DAL.Core;
using MagicVilla.DAL.Entities;
using MagicVilla.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace MagicVilla.DAL.Repositories
{
    public class ApplicationUserRepository : Repository<ApplicationUser>, IApplicationUserRepository
    {
        private readonly MagicVillaContext _context;

        public ApplicationUserRepository(MagicVillaContext context) : base(context)
        {
            _context = context;
        }
    }
}
