﻿using MagicVilla.BLL.Contract;
using MagicVilla.BLL.Dto.Auth;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace MagicVilla.API.Controllers
{
    [Route("api/v{version:apiVersion}/Auth")]
    [ApiController]
    [ApiVersionNeutral]
    public class AuthController : ControllerBase
    {
        private readonly IAuthService _authService;

        public AuthController(IAuthService authService)
        {
            _authService = authService;
        }

        [HttpPost("Login")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Login([FromBody] LoginRequestDto loginRequestDto)
        {
            var serviceResult = await _authService.Login(loginRequestDto);

            if (serviceResult.StatusCode == HttpStatusCode.BadRequest) return BadRequest(serviceResult);
            if (serviceResult.StatusCode == HttpStatusCode.InternalServerError) return Conflict();

            return Ok(serviceResult);
        }

        [HttpPost("Register")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Register([FromBody] RegistrationRequestDto registrationRequestDto)
        {
            var serviceResult = await _authService.Register(registrationRequestDto);
            if (serviceResult.StatusCode == HttpStatusCode.BadRequest) return BadRequest(serviceResult);

            return Ok(serviceResult);
        }
    }
}
