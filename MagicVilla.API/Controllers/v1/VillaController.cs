﻿using MagicVilla.BLL.Contract;
using MagicVilla.BLL.Dto.Villa;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using Microsoft.AspNetCore.Authorization;
using System.Text.Json;

namespace MagicVilla.API.Controllers.v1
{
    [Route("api/v{version:apiVersion}/VillaAPI")]
    [ApiController]
    [ApiVersion("1.0")]
    public class VillaController : ControllerBase
    {
        private readonly IVillaService _villaService;

        public VillaController(IVillaService villaService)
        {
            _villaService = villaService;
        }

        [HttpGet]
        [ResponseCache(CacheProfileName = "Default30")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [Authorize]
        public async Task<IActionResult> GetAll([FromQuery] int? occupancy, string? search, int pageSize = 0, int pageNumber = 1)
        {
            var serviceResult = await _villaService.GetAll(pageSize, pageNumber);

            if (serviceResult.Data == null)
                return Ok(serviceResult);

            if (occupancy > 0)
                serviceResult.Data = serviceResult.Data.Where(x => x.Occupancy == occupancy).ToList();

            if (!string.IsNullOrEmpty(search))
                serviceResult.Data = serviceResult.Data.Where(x => x.Amenity.ToLower().Contains(search) || x.Name.ToLower().Contains(search)).ToList();

            if (serviceResult.StatusCode == HttpStatusCode.InternalServerError)
                return Conflict();

            Pagination pagination = new() { PageSize = pageSize, PageNumber = pageNumber };

            Response.Headers.Add("X-Pagination", JsonSerializer.Serialize(pagination));

            return Ok(serviceResult);
        }

        [HttpGet("{id}")]
        [ResponseCache(CacheProfileName = "Default30")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [Authorize]
        public async Task<IActionResult> GetById(int id)
        {
            var serviceResult = await _villaService.GetById(id);

            if (serviceResult.StatusCode == HttpStatusCode.InternalServerError) return Conflict();

            return Ok(serviceResult);
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> CreateVilla([FromBody] CreateVillaDto createVillaDto)
        {
            return Ok(await _villaService.Create(createVillaDto));
        }

        [HttpPut("update")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> UpdateVilla([FromBody] UpdateVillaDto updateVillaDto)
        {
            return Ok(await _villaService.Update(updateVillaDto));
        }

        [HttpDelete]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> DeleteVilla([FromBody] DeleteVillaDto deleteVillaDto)
        {
            return Ok(await _villaService.Delete(deleteVillaDto));
        }
    }
}
