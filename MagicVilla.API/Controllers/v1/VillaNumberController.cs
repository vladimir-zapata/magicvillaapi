﻿using MagicVilla.BLL.Contract;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using MagicVilla.BLL.Dto.VillaNumber;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MagicVilla.API.Controllers.v1
{
    [Route("api/v{version:apiVersion}/VillaNumberAPI")]
    [ApiController]
    [ApiVersion("1.0")]
    public class VillaNumberController : ControllerBase
    {

        private readonly IVillaNumberService _villaNumberService;

        public VillaNumberController(IVillaNumberService villaNumberService)
        {
            _villaNumberService = villaNumberService;
        }

        [HttpGet]
        [ResponseCache(CacheProfileName = "Default30")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetAllVillas([FromQuery] int pageSize, int pageNumber)
        {
            var serviceResult = await _villaNumberService.GetAll(pageSize, pageNumber);

            if (serviceResult.StatusCode == HttpStatusCode.InternalServerError) return Conflict();

            return Ok(serviceResult);
        }

        [HttpGet("{id}")]
        [ResponseCache(CacheProfileName = "Default30")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetVilla(int id)
        {
            var serviceResult = await _villaNumberService.GetById(id);

            if (serviceResult.StatusCode == HttpStatusCode.InternalServerError) return Conflict();

            return Ok(serviceResult);
        }

        [HttpPost("Create")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> CreateVilla([FromBody] CreateVillaNumberDto createVillaNumberDto)
        {
            var serviceResult = await _villaNumberService.Create(createVillaNumberDto);

            if (serviceResult.StatusCode == HttpStatusCode.InternalServerError) return Conflict();

            return Ok(serviceResult);
        }

        [HttpPut("Update")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> UpdateVilla([FromBody] UpdateVillaNumberDto updateVillaNumberDto)
        {
            var serviceResult = await _villaNumberService.Update(updateVillaNumberDto);

            if (serviceResult.StatusCode == HttpStatusCode.InternalServerError) return Conflict();

            return Ok(serviceResult);
        }

        [HttpDelete("Delete")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> DeleteVilla([FromBody] DeleteVillaNumberDto deleteVillaNumberDto)
        {
            var serviceResult = await _villaNumberService.Delete(deleteVillaNumberDto);

            if (serviceResult.StatusCode == HttpStatusCode.InternalServerError) return Conflict();

            return Ok(serviceResult);
        }
    }
}
