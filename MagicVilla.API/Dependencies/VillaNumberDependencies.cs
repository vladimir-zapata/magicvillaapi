﻿using MagicVilla.BLL.Contract;
using MagicVilla.BLL.Services;
using MagicVilla.DAL.Interfaces;
using MagicVilla.DAL.Repositories;

namespace MagicVilla.API.Dependencies
{
    public static class VillaNumberDependencies
    {
        public static void AddVillaNumberDependencies(this WebApplicationBuilder builder) 
        {
            builder.Services.AddScoped<IVillaNumberRepository, VillaNumberRepository>();
            builder.Services.AddScoped<IVillaNumberService, VillaNumberService>();
        }
    }
}
