﻿using MagicVilla.BLL.Contract;
using MagicVilla.BLL.Services;
using MagicVilla.DAL.Interfaces;
using MagicVilla.DAL.Repositories;

namespace MagicVilla.API.Dependencies
{
    public static class AuthDependencies
    {
        public static void AddAuthDependencies(this WebApplicationBuilder builder)
        {
            builder.Services.AddScoped<IApplicationUserRepository, ApplicationUserRepository>();
            builder.Services.AddScoped<IAuthService, AuthService>();
        }
    }
}
