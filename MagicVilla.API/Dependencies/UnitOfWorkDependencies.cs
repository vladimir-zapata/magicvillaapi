﻿using MagicVilla.DAL.Core;

namespace MagicVilla.API.Dependencies
{
    public static class UnitOfWorkDependencies
    {
        public static void AddUnitOfWorkDependencies(this WebApplicationBuilder builder)
        {
            builder.AddVillaDependencies();
            builder.AddVillaNumberDependencies();
            builder.AddAuthDependencies();

            builder.Services.AddScoped<IUnitOfWork, UnitOfWork>();
        }
    }
}
