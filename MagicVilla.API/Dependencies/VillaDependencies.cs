﻿using MagicVilla.BLL.Contract;
using MagicVilla.BLL.Services;
using MagicVilla.DAL.Interfaces;
using MagicVilla.DAL.Repositories;

namespace MagicVilla.API.Dependencies
{
    public static class VillaDependencies
    {
        public static void AddVillaDependencies(this WebApplicationBuilder builder) 
        {
            builder.Services.AddScoped<IVillaRepository, VillaRepository>();
            builder.Services.AddScoped<IVillaService, VillaService>();
        }
    }
}
