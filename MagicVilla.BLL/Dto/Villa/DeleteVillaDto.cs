﻿using MagicVilla.BLL.Core;
using System.ComponentModel.DataAnnotations;

namespace MagicVilla.BLL.Dto.Villa
{
    public class DeleteVillaDto : BaseDto
    {
        [Required]
        public int Id { get; set; }
    }
}
