﻿using System.ComponentModel.DataAnnotations;
using MagicVilla.BLL.Core;

namespace MagicVilla.BLL.Dto.Villa
{
    public class CreateVillaDto : BaseDto
    {
        [Required]
        [MaxLength(30)]
        public string Name { get; set; } = string.Empty;
        public string Details { get; set; } = string.Empty;
        public double Rate { get; set; }
        public int Sqft { get; set; }
        public int Occupancy { get; set; }
        public string ImageUrl { get; set; } = string.Empty;
        public string Amenity { get; set; } = string.Empty;
    }
}
