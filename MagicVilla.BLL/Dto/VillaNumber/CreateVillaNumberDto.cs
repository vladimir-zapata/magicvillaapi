﻿using MagicVilla.BLL.Core;
using System.ComponentModel.DataAnnotations;

namespace MagicVilla.BLL.Dto.VillaNumber
{
    public class CreateVillaNumberDto : BaseDto
    {
        [Required]
        public int VillaNo { get; set; }
        [Required]
        public int VillaID { get; set; }
        public string SpecialDetails { get; set; }
    }
}
