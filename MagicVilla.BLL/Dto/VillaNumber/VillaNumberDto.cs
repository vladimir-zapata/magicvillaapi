﻿using MagicVilla.BLL.Dto.Villa;
using System.ComponentModel.DataAnnotations;

namespace MagicVilla.BLL.Dto.VillaNumber
{
    public class VillaNumberDto
    {
        [Required]
        public int VillaNo { get; set; }
        [Required]
        public int VillaID { get; set; }
        public string? SpecialDetails { get; set; }
        public VillaDto? Villa { get; set; }
    }
}
