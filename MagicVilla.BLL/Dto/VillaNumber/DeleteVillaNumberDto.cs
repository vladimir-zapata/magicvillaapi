﻿using MagicVilla.BLL.Core;
using System.ComponentModel.DataAnnotations;

namespace MagicVilla.BLL.Dto.VillaNumber
{
    public class DeleteVillaNumberDto : BaseDto
    {
        [Required]
        public int VillaNo { get; set; }
    }
}
