﻿namespace MagicVilla.BLL.Dto.Auth
{
    public class LoginResponseDto
    {
        public string Token { get; set; }
    }
}
