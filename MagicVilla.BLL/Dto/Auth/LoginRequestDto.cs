﻿using System.ComponentModel.DataAnnotations;

namespace MagicVilla.BLL.Dto.Auth
{
    public class LoginRequestDto
    {
        [Required]
        public string UserName { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
