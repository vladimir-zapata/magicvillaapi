﻿using System.Net;

namespace MagicVilla.BLL.Core
{
    public class ServiceResult<T> where T : class
    {
        public HttpStatusCode StatusCode { get; set; }
        public bool IsSuccess { get; set; } = true;
        public List<string>? ErrorMessages { get; set; }
        public T? Data { get; set; }

        public ServiceResult(
            HttpStatusCode statusCode = HttpStatusCode.OK, 
            bool isSuccess = true, 
            List<string>? errorMessages = null, 
            T? data = default
            )
        {
            StatusCode = statusCode;
            IsSuccess = isSuccess;
            ErrorMessages = errorMessages;
            Data = data;
        }
    }
}
