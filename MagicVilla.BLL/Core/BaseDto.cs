﻿using System.ComponentModel.DataAnnotations;

namespace MagicVilla.BLL.Core
{
    public abstract class BaseDto
    {
        [Required]
        public string RequestUser { get; set; } = string.Empty;
    }
}
