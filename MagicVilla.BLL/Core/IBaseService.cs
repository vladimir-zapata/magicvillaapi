﻿using MagicVilla.BLL.Dto.Villa;

namespace MagicVilla.BLL.Core
{
    public interface IBaseService<TDto, in TCreateDto, in TUpdateDto, in TDeleteDto> 
        where TDto : class
        where TCreateDto: class
        where TUpdateDto : class
        where TDeleteDto : class
    {
        Task<ServiceResult<TDto>> GetById(int id);
        Task<ServiceResult<List<TDto>>> GetAll(int pageSize = 0, int pageNumber = 1);
        Task<ServiceResult<TDto>> Create(TCreateDto createDto);
        Task<ServiceResult<TDto>> Update(TUpdateDto updateDto);
        Task<ServiceResult<TDto>> Delete(TDeleteDto deleteDto);
    }
}
