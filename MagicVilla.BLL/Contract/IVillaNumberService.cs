﻿using MagicVilla.BLL.Core;
using MagicVilla.BLL.Dto.VillaNumber;

namespace MagicVilla.BLL.Contract
{
    public interface IVillaNumberService : IBaseService<VillaNumberDto, CreateVillaNumberDto, UpdateVillaNumberDto, DeleteVillaNumberDto> {}
}
