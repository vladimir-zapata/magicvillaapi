﻿using MagicVilla.BLL.Core;
using MagicVilla.BLL.Dto.Villa;

namespace MagicVilla.BLL.Contract
{
    public interface IVillaService : IBaseService<VillaDto, CreateVillaDto, UpdateVillaDto, DeleteVillaDto> {}
}
