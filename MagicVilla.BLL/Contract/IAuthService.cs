﻿using MagicVilla.BLL.Core;
using MagicVilla.BLL.Dto.Auth;
using MagicVilla.DAL.Entities;

namespace MagicVilla.BLL.Contract
{
    public interface IAuthService
    {
        Task<ServiceResult<LoginResponseDto>> Login(LoginRequestDto loginRequestDto);
        Task<ServiceResult<ApplicationUser>> Register(RegistrationRequestDto registrationRequestDto);
    }
}
