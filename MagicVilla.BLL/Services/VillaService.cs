﻿using AutoMapper;
using MagicVilla.BLL.Contract;
using MagicVilla.BLL.Core;
using MagicVilla.BLL.Dto.Villa;
using MagicVilla.DAL.Core;
using MagicVilla.DAL.Entities;
using Microsoft.Extensions.Logging;
using System.Net;

namespace MagicVilla.BLL.Services
{
    public class VillaService : IVillaService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;

        public VillaService(IUnitOfWork unitOfWork, IMapper mapper, ILogger<Villa> logger)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<ServiceResult<VillaDto>> GetById(int id)
        {
            try
            {
                _logger.LogInformation($"Getting villa by id: {id}");

                var villa = await _unitOfWork.GetRepository<Villa>().GetEntityAsync(x => x.Id == id && !x.Deleted);

                return new ServiceResult<VillaDto>(
                    HttpStatusCode.OK, 
                    true, 
                    null, 
                    _mapper.Map<VillaDto>(villa));

            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed get {nameof(Villa)} by id {id}");

                List<string> errors = new List<string>() { ex.Message };

                return new ServiceResult<VillaDto>(HttpStatusCode.InternalServerError, false, errors, null);
            }
        }
        public async Task<ServiceResult<List<VillaDto>>> GetAll(int pageSize, int pageNumber)
        {
            try
            {
                _logger.LogInformation($"Getting all {nameof(Villa)}");

                var villas = _mapper.Map<List<VillaDto>>(await _unitOfWork.GetRepository<Villa>().GetEntitiesAsync(x => !x.Deleted, null, pageSize, pageNumber));

                return new ServiceResult<List<VillaDto>>(HttpStatusCode.OK, true, null, villas);

            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to get all {nameof(Villa)}s");

                List<string> errors = new List<string>() { ex.Message };

                return new ServiceResult<List<VillaDto>>(HttpStatusCode.InternalServerError, false, errors, null);
            }
        }
        public async Task<ServiceResult<VillaDto>> Create(CreateVillaDto createDto)
        {
            try
            {
                _logger.LogInformation($"Creating {nameof(Villa)}");

                var villa = _mapper.Map<Villa>(createDto);

                villa.CreationUser = createDto.RequestUser;
                villa.CreationDate = DateTime.Now;

                await _unitOfWork.GetRepository<Villa>().SaveAsync(villa);

                return new ServiceResult<VillaDto>(HttpStatusCode.OK, false, null, null);

            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to create {nameof(Villa)}");

                List<string> errors = new List<string>() { ex.Message };

                return new ServiceResult<VillaDto>(HttpStatusCode.InternalServerError, false, errors, null);
            }
        }
        public async Task<ServiceResult<VillaDto>> Update(UpdateVillaDto updateDto)
        {
            try
            {
                _logger.LogInformation($"Updating {nameof(Villa)}");

                var villa = await _unitOfWork.GetRepository<Villa>().GetEntityAsync(x => x.Id == updateDto.Id && !x.Deleted);

                if (villa == null) return new ServiceResult<VillaDto>(HttpStatusCode.BadRequest, false, null, null);

                villa.Name = updateDto.Name;
                villa.Details = updateDto.Details;
                villa.Rate = updateDto.Rate;
                villa.Sqft = updateDto.Sqft;
                villa.Occupancy = updateDto.Occupancy;
                villa.ImageUrl = updateDto.ImageUrl;
                villa.Amenity = updateDto.Amenity;
                villa.ModifyUser = updateDto.RequestUser;
                villa.ModifyDate = DateTime.Now;

                await _unitOfWork.GetRepository<Villa>().UpdateAsync(villa);

                return new ServiceResult<VillaDto>(HttpStatusCode.OK, true, null, null);

            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to update {nameof(Villa)}");

                List<string> errors = new List<string>() { ex.Message };

                return new ServiceResult<VillaDto>(HttpStatusCode.InternalServerError, false, errors, null);
            }
        }
        public async Task<ServiceResult<VillaDto>> Delete(DeleteVillaDto deleteDto)
        {
            try
            {
                _logger.LogInformation($"Deleting {nameof(Villa)}");

                var villa = await _unitOfWork.GetRepository<Villa>().GetEntityAsync(x => x.Id == deleteDto.Id && !x.Deleted);

                if (villa == null) return new ServiceResult<VillaDto>(HttpStatusCode.BadRequest, false, null, null);

                villa.Deleted = true;
                villa.DeleteUser = deleteDto.RequestUser;
                villa.DeleteDate = DateTime.Now;

                await _unitOfWork.GetRepository<Villa>().UpdateAsync(villa);

                return new ServiceResult<VillaDto>(HttpStatusCode.OK, true, null, null);

            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to delete {nameof(Villa)}");

                List<string> errors = new List<string>() { ex.Message };

                return new ServiceResult<VillaDto>(HttpStatusCode.InternalServerError, false, errors, null);
            }
        }
    }
}
