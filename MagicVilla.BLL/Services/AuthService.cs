﻿using System.IdentityModel.Tokens.Jwt;
using MagicVilla.BLL.Contract;
using MagicVilla.BLL.Core;
using MagicVilla.BLL.Dto.Auth;
using MagicVilla.BLL.Dto.Villa;
using MagicVilla.DAL.Core;
using MagicVilla.DAL.Entities;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using AutoMapper;
using System.Net;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Identity;

namespace MagicVilla.BLL.Services
{
    public class AuthService : IAuthService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly string _secretKey;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;

        public AuthService(IUnitOfWork unitOfWork,
            UserManager<ApplicationUser> userManager, 
            RoleManager<IdentityRole> roleManager,
            IConfiguration configuration, 
            IMapper mapper, 
            ILogger<AuthService> logger)
        {
            _unitOfWork = unitOfWork;
            _userManager = userManager;
            _roleManager = roleManager;
            _secretKey = configuration.GetSection("ApiSettings:Secret").Value;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<ServiceResult<LoginResponseDto>> Login(LoginRequestDto loginRequestDto)
        {
            try
            {
                var user = await _unitOfWork.GetRepository<ApplicationUser>().GetEntityAsync(x => x.UserName == loginRequestDto.UserName);
                if (user == null) return new ServiceResult<LoginResponseDto>(HttpStatusCode.BadRequest, false, new List<string>() { "Invalid username or password" }, null);

                bool isValidPassword = await _userManager.CheckPasswordAsync(user, loginRequestDto.Password);
                if (isValidPassword == false) return new ServiceResult<LoginResponseDto>(HttpStatusCode.BadRequest, false, new List<string>() { "Invalid username or password" }, null);

                var roles = await _userManager.GetRolesAsync(user);

                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes(_secretKey);

                var tokenDescriptor = new SecurityTokenDescriptor()
                {
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                        new Claim(ClaimTypes.Name, user.Id.ToString()),
                        new Claim(ClaimTypes.Role, roles.FirstOrDefault() ?? "")
                    }),
                    Expires = DateTime.UtcNow.AddDays(7),
                    SigningCredentials = new(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                };

                var token = tokenHandler.CreateToken(tokenDescriptor);

                return new ServiceResult<LoginResponseDto>(HttpStatusCode.OK, false, null, new LoginResponseDto() { Token = tokenHandler.WriteToken(token)});

            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to login {nameof(User)}");

                List<string> errors = new List<string>() { ex.Message };

                return new ServiceResult<LoginResponseDto>(HttpStatusCode.InternalServerError, false, errors, null);
            }
        }

        public async Task<ServiceResult<ApplicationUser>> Register(RegistrationRequestDto registrationRequestDto)
        {
            ApplicationUser user = new()
            {
                UserName = registrationRequestDto.UserName,
                Email = registrationRequestDto.UserName,
                NormalizedEmail = registrationRequestDto.UserName.ToUpper(),
                Name = registrationRequestDto.UserName,
            };

            try
            {
                var userExists = await _unitOfWork.GetRepository<ApplicationUser>().ExistsAsync(x => x.UserName == registrationRequestDto.UserName);
                if (userExists) return new ServiceResult<ApplicationUser>(HttpStatusCode.BadRequest, false, new List<string>() { "User already exists" }, null);

                var result = await _userManager.CreateAsync(user, registrationRequestDto.Password);

                if (result.Succeeded) 
                {
                    if (!_roleManager.RoleExistsAsync("admin").GetAwaiter().GetResult()) 
                    {
                        await _roleManager.CreateAsync(new IdentityRole("admin"));
                        await _roleManager.CreateAsync(new IdentityRole("customer"));
                    }
                    await _userManager.AddToRoleAsync(user, "admin");
                }

                return new ServiceResult<ApplicationUser>(HttpStatusCode.OK, true, null, null);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to register {nameof(User)}");

                List<string> errors = new List<string>() { ex.Message };

                return new ServiceResult<ApplicationUser>(HttpStatusCode.InternalServerError, false, errors, null);
            }
        }
    }
}
