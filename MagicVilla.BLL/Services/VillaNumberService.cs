﻿using AutoMapper;
using MagicVilla.BLL.Contract;
using MagicVilla.BLL.Core;
using MagicVilla.BLL.Dto.Villa;
using MagicVilla.BLL.Dto.VillaNumber;
using MagicVilla.DAL.Core;
using MagicVilla.DAL.Entities;
using Microsoft.Extensions.Logging;
using System.Net;

namespace MagicVilla.BLL.Services
{
    public class VillaNumberService : IVillaNumberService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;

        public VillaNumberService(IUnitOfWork unitOfWork, IMapper mapper, ILogger<VillaNumberService> logger)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<ServiceResult<List<VillaNumberDto>>> GetAll(int pageSize, int pageNumber)
        {
            try
            {
                _logger.LogInformation($"Getting all villa numbers");

                var villaNumbers = _mapper
                    .Map<List<VillaNumberDto>>(await _unitOfWork.GetRepository<VillaNumber>().GetEntitiesAsync(x => !x.Deleted, null, pageSize, pageNumber));

                return new ServiceResult<List<VillaNumberDto>>(
                    HttpStatusCode.OK,
                    true,
                    null,
                    villaNumbers);

            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed get villa numbers");

                List<string> errors = new List<string>() { ex.Message };

                return new ServiceResult<List<VillaNumberDto>>(HttpStatusCode.InternalServerError, false, errors, null);
            }
        }

        public async Task<ServiceResult<VillaNumberDto>> GetById(int id)
        {
            try
            {
                _logger.LogInformation($"Getting villa by id");

                var villaNumber = _mapper
                    .Map<VillaNumberDto>(await _unitOfWork.GetRepository<VillaNumber>().GetEntityAsync(x => x.VillaNo == id && !x.Deleted));

                return new ServiceResult<VillaNumberDto>(
                    HttpStatusCode.OK,
                    true,
                    null,
                    villaNumber);

            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed get villa number by id");

                List<string> errors = new List<string>() { ex.Message };

                return new ServiceResult<VillaNumberDto>(HttpStatusCode.InternalServerError, false, errors, null);
            }
        }

        public async Task<ServiceResult<VillaNumberDto>> Create(CreateVillaNumberDto createDto)
        {
            try
            {
                _logger.LogInformation($"Creating {nameof(VillaNumber)}");

                var villa = _mapper.Map<VillaNumber>(createDto);

                villa.CreationUser = createDto.RequestUser;
                villa.CreationDate = DateTime.Now;

                await _unitOfWork.GetRepository<VillaNumber>().SaveAsync(villa);

                return new ServiceResult<VillaNumberDto>(HttpStatusCode.OK, true, null, null);

            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to create {nameof(VillaNumber)}");

                List<string> errors = new List<string>() { ex.Message };

                return new ServiceResult<VillaNumberDto>(HttpStatusCode.InternalServerError, false, errors, null);
            }
        }

        public async Task<ServiceResult<VillaNumberDto>> Update(UpdateVillaNumberDto updateDto)
        {
            try
            {
                _logger.LogInformation($"Updating {nameof(VillaNumberDto)}");

                var villaNumber = await _unitOfWork.GetRepository<VillaNumber>().GetEntityAsync(x => x.VillaNo == updateDto.VillaNo && !x.Deleted);
                var villa = await _unitOfWork.GetRepository<Villa>().GetEntityAsync(x => x.Id == updateDto.VillaID && !x.Deleted);

                if(villa == null) return new ServiceResult<VillaNumberDto>(HttpStatusCode.BadRequest, false, new List<string>() { "Invalid villa id" }, null);
                if (villaNumber == null) return new ServiceResult<VillaNumberDto>(HttpStatusCode.BadRequest, false, null, null);

                villaNumber.VillaID = updateDto.VillaID;
                villaNumber.SpecialDetails = updateDto.SpecialDetails;
                villaNumber.ModifyUser = updateDto.RequestUser;
                villaNumber.ModifyDate = DateTime.Now;

                await _unitOfWork.GetRepository<VillaNumber>().UpdateAsync(villaNumber);

                return new ServiceResult<VillaNumberDto>(HttpStatusCode.OK, true, null, null);

            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to update {nameof(VillaNumber)}");

                List<string> errors = new List<string>() { ex.Message };

                return new ServiceResult<VillaNumberDto>(HttpStatusCode.InternalServerError, false, errors, null);
            }
        }

        public async Task<ServiceResult<VillaNumberDto>> Delete(DeleteVillaNumberDto deleteDto)
        {
            try
            {
                _logger.LogInformation($"Deleting {nameof(VillaNumber)}");

                var villa = await _unitOfWork.GetRepository<VillaNumber>().GetEntityAsync(x => x.VillaNo == deleteDto.VillaNo && !x.Deleted);

                if (villa == null) return new ServiceResult<VillaNumberDto>(HttpStatusCode.BadRequest, false, null, null);

                villa.Deleted = true;
                villa.DeleteUser = deleteDto.RequestUser;
                villa.DeleteDate = DateTime.Now;

                await _unitOfWork.GetRepository<VillaNumber>().RemoveAsync(villa);

                return new ServiceResult<VillaNumberDto>(HttpStatusCode.OK, true, null, null);

            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to delete {nameof(VillaNumber)}");

                List<string> errors = new List<string>() { ex.Message };

                return new ServiceResult<VillaNumberDto>(HttpStatusCode.InternalServerError, false, errors, null);
            }
        }
    }
}
