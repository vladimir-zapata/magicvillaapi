﻿using AutoMapper;
using MagicVilla.BLL.Dto.Auth;
using MagicVilla.BLL.Dto.Villa;
using MagicVilla.BLL.Dto.VillaNumber;
using MagicVilla.DAL.Entities;

namespace MagicVilla.BLL.Mappings
{
    public class MappingConfig : Profile
    {
        public MappingConfig()
        {
            CreateMap<Villa, VillaDto>().ReverseMap();
            CreateMap<Villa, CreateVillaDto>().ReverseMap();
            CreateMap<Villa, UpdateVillaDto>().ReverseMap();

            CreateMap<VillaNumber, VillaNumberDto>().ReverseMap();
            CreateMap<VillaNumber, CreateVillaNumberDto>().ReverseMap();
            CreateMap<VillaNumber, UpdateVillaNumberDto>().ReverseMap();

            CreateMap<User, RegistrationRequestDto>().ReverseMap();
        }
    }
}
